//
//  UIViewExtension.swift
//  Gulberenk
//
//  Created by Seyda Kalyoncu on 8.05.2020.
//  Copyright © 2020 Seyda Kalyoncu. All rights reserved.
//

import UIKit

extension UIView {
    func addShadow(toAbove: Bool = false, opacity: Float = 0.09) {
        layer.shadowOffset = CGSize(width: 1, height: toAbove ? -1 : 1)
        layer.shadowRadius = 4
        layer.shadowColor = UIColor.black.cgColor
        layer.shadowOpacity = opacity
        layer.masksToBounds = false
    }


}
