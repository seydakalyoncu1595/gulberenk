//
//  MusicTableViewCell.swift
//  Gulberenk
//
//  Created by Seyda Kalyoncu on 18.04.2020.
//  Copyright © 2020 Seyda Kalyoncu. All rights reserved.
//

import UIKit

class MusicTableViewCell: UITableViewCell {
    
    @IBOutlet weak var songImageView: UIImageView!
    @IBOutlet weak var songNameLabel: UILabel!
    @IBOutlet weak var songArtistLabel: UILabel!
    @IBOutlet weak var songComposerAuthorLabel: UILabel!
    @IBOutlet weak var songArrangerLabel: UILabel!
    @IBOutlet weak var musicView: UIView!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        songImageView.clipsToBounds = true
        selectionStyle = .none
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
