//
//  AlbumsCollectionViewCell.swift
//  Gulberenk
//
//  Created by Seyda Kalyoncu on 14.04.2020.
//  Copyright © 2020 Seyda Kalyoncu. All rights reserved.
//

import UIKit

class AlbumsCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var albumName: UILabel!
    @IBOutlet weak var albumImage: UIImageView!

}
