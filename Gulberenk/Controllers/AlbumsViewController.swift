//
//  AlbumsViewController.swift
//  Gulberenk
//
//  Created by Seyda Kalyoncu on 13.04.2020.
//  Copyright © 2020 Seyda Kalyoncu. All rights reserved.
//

import UIKit

class AlbumsViewController: UIViewController, UICollectionViewDataSource, UICollectionViewDelegate {

    @IBOutlet weak var albumsCollectionView: UICollectionView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        jsonParsingFromFile()
        
        albumsCollectionView.delegate = self
        albumsCollectionView.dataSource = self

    }
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }

    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        APPDELEGATE.albumItems.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "AlbumsCollectionViewCell", for: indexPath) as! AlbumsCollectionViewCell
        
        let item:AlbumItems = APPDELEGATE.albumItems[indexPath.row]
        cell.albumName.text = item.name
        cell.albumImage.image = UIImage(named: item.image)
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let item:AlbumItems = APPDELEGATE.albumItems[indexPath.row]
        let controller = UIStoryboard.init(name: "Musics", bundle: nil).instantiateViewController(withIdentifier: "MusicsViewController") as! MusicsViewController
        controller.albumImage.image = UIImage(named: item.image)
        controller.musicItems = item.musicItem
        controller.albumName = item.name
        self.navigationController?.pushViewController(controller, animated: true)
    }
    
    
    func jsonParsingFromFile(resourceName: String = "Albums") {
        do {
            if let file = Bundle.main.url(forResource: resourceName, withExtension: "json")
            {
                let data = try Data(contentsOf: file)
                let json = try JSONSerialization.jsonObject(with: data, options: [])
                if let object = json as? [String: Any] {
                    // json is a dictionary
                    print(object)
                } else if let object = json as? [Any] {
                    // json is an array
                    for dict in object {
                        let item = AlbumItems()
                        
                        item.name = (dict as AnyObject).value(forKey: "albumName") as! String
                        item.actionId = (dict as AnyObject).value(forKey: "actionId") as! Int
                        item.image = (dict as AnyObject).value(forKey: "albumImage") as! String
                        
                        if (dict as AnyObject).value(forKey: "musicItems") != nil
                        {
                            for subdict in (dict as! NSDictionary).value(forKey: "musicItems") as! Array<Any>
                            {
                                let subitem = MusicItems()
                                subitem.songName = (subdict as AnyObject).value(forKey: "songName") as! String
                                subitem.songArtist = (subdict as AnyObject).value(forKey: "songArtist") as! String
                                subitem.songComposerAuthor = (subdict as AnyObject).value(forKey: "songComposerAuthor") as! String
                                subitem.songArranger = (subdict as AnyObject).value(forKey: "songArranger") as! String
                                
                                item.musicItem.append(subitem)
                            }
                        }
                        
                        APPDELEGATE.albumItems.append(item)
                        
                        if item.musicItem.count > 0
                        {
                            for subitem in item.musicItem
                            {
                                APPDELEGATE.musicItems.append(subitem)

                            }
                        }
                    }
                    albumsCollectionView.reloadData()
                    print(object)
                } else {
                    //                    print("JSON is invalid")
                }
            } else {
                //                print("no file")
            }
        } catch {
            print(error.localizedDescription)
        }
    }

}

private let separatorDecorationView = "separator"

final class CustomFlowLayout: UICollectionViewFlowLayout {

    override func awakeFromNib() {
        super.awakeFromNib()
        register(SeparatorView.self, forDecorationViewOfKind: separatorDecorationView)
    }

    override func layoutAttributesForElements(in rect: CGRect) -> [UICollectionViewLayoutAttributes]? {
        let layoutAttributes = super.layoutAttributesForElements(in: rect) ?? []
        let lineWidth = CGFloat(2)

        var decorationAttributes: [UICollectionViewLayoutAttributes] = []

        // skip first cell
        for layoutAttribute in layoutAttributes where layoutAttribute.indexPath.item > 1 {
            let separatorAttribute = UICollectionViewLayoutAttributes(forDecorationViewOfKind: separatorDecorationView,
                                                                      with: layoutAttribute.indexPath)
            let cellFrame = layoutAttribute.frame
            separatorAttribute.frame = CGRect(x: cellFrame.origin.x,
                                              y: cellFrame.origin.y - lineWidth,
                                              width: cellFrame.size.width,
                                              height: lineWidth)
            separatorAttribute.zIndex = Int.max
            decorationAttributes.append(separatorAttribute)
        }

        return layoutAttributes + decorationAttributes
    }

}

private final class SeparatorView: UICollectionReusableView {
    override init(frame: CGRect) {
        super.init(frame: frame)
        self.backgroundColor = .black
    }

    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    override func apply(_ layoutAttributes: UICollectionViewLayoutAttributes) {
        self.frame = layoutAttributes.frame
    }
}
