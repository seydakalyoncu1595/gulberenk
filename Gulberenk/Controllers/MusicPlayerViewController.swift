//
//  MusicPlayerViewController.swift
//  Gulberenk
//
//  Created by Seyda Kalyoncu on 25.04.2020.
//  Copyright © 2020 Seyda Kalyoncu. All rights reserved.
//

import UIKit
import AVFoundation

class MusicPlayerViewController: UIViewController {
    
    @IBOutlet weak var songImageView: UIImageView!
    @IBOutlet weak var songNameLabel: UILabel!
    @IBOutlet weak var songArtistNameLabel: UILabel!
    @IBOutlet weak var songSlider: UISlider!
    @IBOutlet weak var previousSongButton: UIButton!
    @IBOutlet weak var playPauseSongButton: UIButton!
    @IBOutlet weak var nextSongButton: UIButton!
    @IBOutlet weak var scrollView: UIScrollView!
    
    var position: Int = 0
    var musicItems = Array<MusicItems>()
    var songImage = UIImageView()
    
    var player: AVAudioPlayer?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let backButton = UIBarButtonItem()
        backButton.title = ""
        backButton.tintColor = .white
        self.navigationController?.navigationBar.topItem?.backBarButtonItem = backButton
        
        setupScrollView()
        setupPlayer()
        
        if let player = player {
            songSlider.maximumValue = Float(player.duration)
        }
        
        Timer.scheduledTimer(timeInterval: 0.1, target: self, selector: #selector(updateSlider), userInfo: nil, repeats: true)
    }
    
    func setupScrollView() {
        let contentViewSize = CGSize(width: self.view.frame.size.width, height: self.view.frame.size.height - (self.navigationController?.navigationBar.frame.size.height)!)
        scrollView.backgroundColor = .clear
        scrollView.frame = view.bounds
        scrollView.contentSize = contentViewSize
        scrollView.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        scrollView.bounces = true
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        if let player = player {
            player.stop()
        }
    }
    
    func setupPlayer() {
        let song = musicItems[position]
        let urlString = Bundle.main.path(forResource: song.songName, ofType: "mp3")
        
        do {
            try AVAudioSession.sharedInstance().setMode(.default)
            try AVAudioSession.sharedInstance().setActive(true, options: .notifyOthersOnDeactivation)
            try AVAudioSession.sharedInstance().setCategory(.playback)
            
            guard let urlString = urlString else {
                print("urlstring is nil")
                return
            }
            
            player = try AVAudioPlayer(contentsOf: URL(fileURLWithPath: urlString))
            player?.delegate = self
            
            guard let player = player else {
                print("player is nil")
                return
            }
            
            player.play()
        } catch {
            print("error occurred")
        }
        setSongInfo(song: song)
    }
    
    func setSongInfo(song: MusicItems) {
        songImageView.image = songImage.image
        songNameLabel.text = song.songName
        songArtistNameLabel.text = song.songArtist
        
        playPauseSongButton.setBackgroundImage(UIImage(systemName: "pause.circle.fill"), for: .normal)
    }
    
    @objc func updateSlider() {
        if let player = player {
            songSlider.value = Float(player.currentTime)
        }
    }
    
    
    @IBAction func onChangedSongSlider(_ sender: Any) {
        player?.stop()
        player?.currentTime = TimeInterval(songSlider.value)
        player?.prepareToPlay()
        player?.play()
    }
    
    @IBAction func onPreviousSongButtonPressed(_ sender: Any) {
        if position > 0 {
            position = position - 1
            player?.stop()
            setupPlayer()
        }
    }
    
    @IBAction func onPlayPauseSongButtonPressed(_ sender: Any) {
        if player?.isPlaying == true {
            player?.pause()
            playPauseSongButton.setBackgroundImage(UIImage(systemName: "play.circle.fill"), for: .normal)
        }
        else {
            // play
            player?.play()
            playPauseSongButton.setBackgroundImage(UIImage(systemName: "pause.circle.fill"), for: .normal)
        }
    }
    
    
    @IBAction func onNextSongButtonPressed() {
        if position < (musicItems.count - 1) {
            position = position + 1
            player?.stop()
            setupPlayer()
        }
    }
}

extension MusicPlayerViewController: AVAudioPlayerDelegate {
    
    func audioPlayerDidFinishPlaying(_ player: AVAudioPlayer, successfully flag: Bool) {
        onNextSongButtonPressed()
    }
}
