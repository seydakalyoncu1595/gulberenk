//
//  MusicsViewController.swift
//  Gulberenk
//
//  Created by Seyda Kalyoncu on 18.04.2020.
//  Copyright © 2020 Seyda Kalyoncu. All rights reserved.
//

import UIKit

class MusicsViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {
    
    @IBOutlet weak var musicTableView: UITableView!
    
    var albumImage = UIImageView()
    var albumName = String()
    var musicItems = Array<MusicItems>()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = albumName
        
        let backButton = UIBarButtonItem()
        backButton.title = ""
        backButton.tintColor = .white
        self.navigationController?.navigationBar.topItem?.backBarButtonItem = backButton
        
        musicTableView.delegate = self
        musicTableView.dataSource = self

    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        musicItems.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "MusicTableViewCell", for: indexPath) as! MusicTableViewCell
        
        let item:MusicItems = musicItems[indexPath.row]
        cell.songNameLabel.text = item.songName
        cell.songArtistLabel.text = item.songArtist
        cell.songComposerAuthorLabel.text = item.songComposerAuthor
        cell.songArrangerLabel.text = item.songArranger
        cell.songImageView.image = albumImage.image
        cell.musicView.addShadow()
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let controller = UIStoryboard.init(name: "Musics", bundle: nil).instantiateViewController(withIdentifier: "MusicPlayerViewController") as! MusicPlayerViewController
        controller.songImage.image = albumImage.image
        controller.musicItems = musicItems
        controller.position = indexPath.row
        controller.title = albumName
        
        self.navigationController?.pushViewController(controller, animated: true)
    }

}
