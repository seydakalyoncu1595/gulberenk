//
//  MusicItems.swift
//  Gulberenk
//
//  Created by Seyda Kalyoncu on 18.04.2020.
//  Copyright © 2020 Seyda Kalyoncu. All rights reserved.
//

import UIKit

class MusicItems: NSObject {
    
    var songName = String()
    var songArtist = String()
    var songComposerAuthor = String()
    var songArranger = String()

}
