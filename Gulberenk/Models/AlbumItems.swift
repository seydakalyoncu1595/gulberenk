//
//  AlbumItems.swift
//  Gulberenk
//
//  Created by Seyda Kalyoncu on 14.04.2020.
//  Copyright © 2020 Seyda Kalyoncu. All rights reserved.
//

import UIKit

class AlbumItems: NSObject {
    
    var name = String()
    var image = String()
    var actionId = Int()
    var musicItem = Array<MusicItems>()
    
}
